import pytest

from app.data_access.models.telegram_user import TelegramUser


@pytest.mark.django_db
@pytest.mark.smoke
def test_view(client):
    TelegramUser.objects.create(id=123, first_name="test")
    response = client.get("/api/users/123")
    assert response.status_code == 401

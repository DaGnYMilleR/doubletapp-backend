import pytest
import requests
import telegram

from app.telegram_bot.bot import Bot
from config.settings import TELEGRAM_BOT_TOKEN


@pytest.mark.smoke
def test_bot():
    bot = Bot(TELEGRAM_BOT_TOKEN)
    update = telegram.Update.de_json({}, bot)
    bot.dispatcher.process_update(update)


@pytest.mark.smoke
def test_bot_works():
    url = f"https://api.telegram.org/bot{TELEGRAM_BOT_TOKEN}/getMe"
    response = requests.get(url)
    assert response.status_code == 200

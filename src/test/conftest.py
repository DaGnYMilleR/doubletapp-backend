import pytest

from app.data_access.models.bank_account import BankAccount
from app.data_access.models.card import Card
from app.data_access.models.telegram_user import TelegramUser


@pytest.fixture(scope="function")
def test_user(id=1, username="nick", first_name="firstname", last_name="lastname"):
    return TelegramUser.objects.create(id=id, username=username, first_name=first_name, last_name=last_name)


@pytest.fixture(scope="function")
def test_bank_account(test_user: TelegramUser, id=1):
    return BankAccount.objects.create(id=id, user=test_user)


@pytest.fixture(scope="function")
def test_card(test_bank_account: BankAccount, id=1):
    return Card.objects.create(id=id, bank_account=test_bank_account, money=10)

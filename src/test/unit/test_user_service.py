from test.unit.conftest import CardFactory

import pytest

from app.data_access.models.bank_account import BankAccount
from app.data_access.models.card import Card
from app.data_access.models.telegram_user import TelegramUser
from app.domain.services.password_service import PasswordService
from app.domain.services.user_service import UserService
from app.infrastructure.message_constants.error_messages import (
    USER_BANK_ACCOUNT_NOT_FOUND,
    USER_CARD_NOT_FOUND,
    USER_NOT_FOUND,
)

password_service = PasswordService()
sut = UserService(password_service)


@pytest.mark.django_db
@pytest.mark.unit
def test_should_not_create_user_with_same_id_twice():
    id = 1
    create_user_result = sut.create_user(id, "test", "test", "test")
    create_same_user_result = sut.create_user(id, "another", "another", "another")

    assert create_user_result.success
    assert not create_same_user_result.success


@pytest.mark.django_db
@pytest.mark.unit
def test_should_create_user_without_username():
    create_user_result = sut.create_user(1, None, None, None)

    assert create_user_result.success


@pytest.mark.django_db
@pytest.mark.unit
def test_should_return_exist_user(test_user: TelegramUser):
    get_user_result = sut.get_user(test_user.id)

    assert get_user_result.success
    assert get_user_result.value.id == test_user.id


@pytest.mark.django_db
@pytest.mark.unit
def test_should_return_not_found_when_user_not_exists():
    get_user_result = sut.get_user(1)

    assert not get_user_result.success
    assert get_user_result.error_message == USER_NOT_FOUND


@pytest.mark.django_db
@pytest.mark.unit
def test_should_return_exists_card(test_card: Card):
    get_card_result = sut.get_card(test_card.id)

    assert get_card_result.success
    assert get_card_result.value.id == test_card.id


@pytest.mark.django_db
@pytest.mark.unit
def test_get_card_should_return_not_found_when_card_not_exists():
    get_card_result = sut.get_card(1)

    assert not get_card_result.success
    assert get_card_result.error_message == USER_CARD_NOT_FOUND


@pytest.mark.django_db
@pytest.mark.unit
def test_should_return_card_balance(test_card: Card):
    card_balance = 100.00
    test_card.money = card_balance
    test_card.save()

    balance = sut.get_card_balance(test_card.bank_account.user.id, test_card.id)

    assert balance.success
    assert balance.value == card_balance


@pytest.mark.django_db
@pytest.mark.unit
def test_get_card_balance_should_return_not_found_when_card_not_exists(test_bank_account: BankAccount):
    balance = sut.get_card_balance(test_bank_account.user.id, 1)

    assert not balance.success
    assert balance.error_message == USER_CARD_NOT_FOUND


@pytest.mark.django_db
@pytest.mark.unit
def test_should_return_bank_account_sum(test_bank_account: BankAccount):
    card1 = CardFactory.create_card(1, test_bank_account, 100)
    card2 = CardFactory.create_card(2, test_bank_account, 100)
    card3 = CardFactory.create_card(3, test_bank_account, 100)

    sum = sut.get_bank_account_balance(test_bank_account.user.id)

    assert sum.success
    assert sum.value == card1.money + card2.money + card3.money


@pytest.mark.django_db
@pytest.mark.unit
def test_should_return_not_found_when_user_has_not_bank_account(test_user: TelegramUser):
    sum = sut.get_bank_account_balance(test_user.id)

    assert not sum.success
    assert sum.error_message == USER_BANK_ACCOUNT_NOT_FOUND


@pytest.mark.django_db
@pytest.mark.unit
def test_is_card_belongs_to_user_should_return_true_if_card_belongs_to_user(test_card: Card):
    assert sut.is_card_belongs_to_user(test_card.bank_account.user.id, test_card.id)


@pytest.mark.django_db
@pytest.mark.unit
def test_is_card_belongs_to_user_should_return_false_if_card_does_not_belongs_to_user(test_card: Card):
    assert not sut.is_card_belongs_to_user(444, test_card.id)

import pytest

from app.data_access.models.telegram_user import TelegramUser
from app.domain.services.password_service import PasswordService
from app.domain.services.phone_number_service import PhoneNumberService
from app.domain.services.user_service import UserService
from app.infrastructure.message_constants.error_messages import INVALID_PHONE_NUMBER_FORMAT
from app.infrastructure.message_constants.response_messages import PHONE_SAVED

password_service = PasswordService()
user_service = UserService(password_service)
sut = PhoneNumberService(user_service)


@pytest.mark.django_db
@pytest.mark.unit
def test_should_set_correct_user_phone(test_user: TelegramUser):
    phone = "7777777777"
    set_phone_result = sut.set_user_phone(test_user.id, phone)
    user = user_service.get_user(test_user.id)

    assert set_phone_result.success
    assert set_phone_result.value == PHONE_SAVED
    assert user.value.phone_number == phone


@pytest.mark.django_db
@pytest.mark.unit
def test_should_not_set_incorrect_user_phone(test_user: TelegramUser):
    phone = "incorrect"
    set_phone_result = sut.set_user_phone(test_user.id, phone)
    user = user_service.get_user(test_user.id)

    assert not set_phone_result.success
    assert set_phone_result.error_message == INVALID_PHONE_NUMBER_FORMAT
    assert user.value.phone_number is None

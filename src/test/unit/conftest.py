from app.data_access.models.bank_account import BankAccount
from app.data_access.models.card import Card


class CardFactory:
    @staticmethod
    def create_card(id: int, bank_account: BankAccount, amount: float):
        return Card.objects.create(id=id, bank_account=bank_account, money=amount)

import pytest

from app.infrastructure.parsers.try_parse_float import try_parse_float
from app.infrastructure.parsers.try_parse_int import try_parse_int


@pytest.mark.unit
def test_parse_int_should_return_false_when_not_int():
    result, success = try_parse_int("aaa")
    assert not success


@pytest.mark.unit
def test_parse_int_should_return_false_when_float():
    result, success = try_parse_int("123.0")
    assert not success


@pytest.mark.unit
def test_parse_int_should_return_true_when_int():
    result, success = try_parse_int("123")
    assert success
    assert result == 123


@pytest.mark.unit
def test_parse_float_should_return_false_when_not_float():
    result, success = try_parse_float("aaa")
    assert not success


@pytest.mark.unit
def test_parse_float_should_return_false_when_float():
    result, success = try_parse_float("123.3")
    assert success
    assert result == 123.3


@pytest.mark.unit
def test_parse_float_should_return_true_when_int():
    result, success = try_parse_float("123")
    assert success
    assert result == 123

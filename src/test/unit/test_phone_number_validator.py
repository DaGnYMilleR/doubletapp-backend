import pytest

from app.domain.validators.phone_number_validator import PhoneNumberValidator


@pytest.mark.parametrize("number", ["aaa", "."])
@pytest.mark.unit
def test_should_return_false_when_no_digits(number: str):
    assert not PhoneNumberValidator.is_valid(number)


@pytest.mark.unit
def test_should_return_false_when_less_than_eleven_digits():
    assert not PhoneNumberValidator.is_valid("11111")


@pytest.mark.unit
def test_should_return_false_when_comma_first():
    assert not PhoneNumberValidator.is_valid(",79224463307")


@pytest.mark.unit
def test_should_return_true_when_right_format():
    assert PhoneNumberValidator.is_valid("+79224463307")


@pytest.mark.unit
def test_should_return_true_when_contains_brackets():
    assert PhoneNumberValidator.is_valid("7(902)4463337")


@pytest.mark.parametrize("number", ["7-(902)-44-63-337", "7-(902)-44-63337", "7-(902)4463337"])
@pytest.mark.unit
def test_should_return_true_when_contains_hyphen(number: str):
    assert PhoneNumberValidator.is_valid(number)

from unittest.mock import MagicMock, PropertyMock

import pytest


@pytest.fixture(scope="function")
def test_telegram_update() -> MagicMock:
    user = MagicMock()
    type(user).id = PropertyMock(return_value=123)
    type(user).username = PropertyMock(return_value="username")
    type(user).first_name = PropertyMock(return_value="first_name")
    type(user).last_name = PropertyMock(return_value="last_name")
    message_mock = MagicMock()
    message_mock.reply_text = MagicMock(return_value=None)
    type(message_mock).from_user = PropertyMock(return_value=user)
    update_mock = MagicMock()
    type(update_mock).message = PropertyMock(return_value=message_mock)
    return update_mock


@pytest.fixture(scope="function")
def test_callback_context_with_phone() -> MagicMock:
    context = MagicMock()
    type(context).args = PropertyMock(return_value=["77777777777"])
    return context


def mock_callback(*args) -> MagicMock:
    context = MagicMock()
    type(context).args = PropertyMock(return_value=args)
    return context

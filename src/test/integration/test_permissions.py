from test.integration.conftest import mock_callback
from typing import Callable

import pytest
from injector import Injector
from telegram import Update
from telegram.ext import CallbackContext

from app.infrastructure.message_constants.error_messages import NO_PERMISSIONS
from app.infrastructure.message_constants.response_messages import PHONE_SAVED
from app.telegram_bot.handlers.base_commands_handlers import BaseHandlers
from app.telegram_bot.handlers.favorite_users_handlers import FavoriteUsersHandlers
from app.telegram_bot.handlers.money_transfer_handlers import MoneyTransferHandlers

injector = Injector()
base_commands_handlers = injector.get(BaseHandlers)
favorite_users_handlers = injector.get(FavoriteUsersHandlers)
money_transfer_handlers = injector.get(MoneyTransferHandlers)


class TestPermissions:
    @pytest.mark.django_db
    @pytest.mark.integration
    def test_start(self, test_telegram_update: Update):
        base_commands_handlers.cmd_start(test_telegram_update, None)
        test_telegram_update.message.reply_text.assert_called_once()

    @pytest.mark.django_db
    @pytest.mark.integration
    def test_set_phone(self, test_telegram_update: Update):
        callback = mock_callback("77777777777")
        base_commands_handlers.cmd_start(test_telegram_update, None)

        base_commands_handlers.cmd_set_phone(test_telegram_update, callback)

        test_telegram_update.message.reply_text.assert_called_with(PHONE_SAVED)

    @pytest.mark.django_db
    @pytest.mark.integration
    def test_me(self, test_telegram_update: Update):
        assert_returns_permissions_required_message(base_commands_handlers.cmd_me, test_telegram_update, None)

    @pytest.mark.django_db
    @pytest.mark.integration
    def test_add_to_favorites(self, test_telegram_update: Update):
        assert_returns_permissions_required_message(
            favorite_users_handlers.cmd_add_to_favorites, test_telegram_update, None
        )

    @pytest.mark.django_db
    @pytest.mark.integration
    def test_get_favorites_list(self, test_telegram_update: Update):
        assert_returns_permissions_required_message(
            favorite_users_handlers.cmd_get_favorites_list, test_telegram_update, None
        )

    @pytest.mark.django_db
    @pytest.mark.integration
    def test_delete_favorite(self, test_telegram_update: Update):
        assert_returns_permissions_required_message(
            favorite_users_handlers.cmd_delete_favorite, test_telegram_update, None
        )

    @pytest.mark.django_db
    @pytest.mark.integration
    def test_money(self, test_telegram_update: Update):
        assert_returns_permissions_required_message(money_transfer_handlers.cmd_money, test_telegram_update, None)

    @pytest.mark.django_db
    @pytest.mark.integration
    def test_transfer_user_money(self, test_telegram_update: Update):
        assert_returns_permissions_required_message(
            money_transfer_handlers.cmd_transfer_user_money, test_telegram_update, None
        )


def assert_returns_permissions_required_message(
    func: Callable[[Update, CallbackContext], None], update: Update, context: CallbackContext
):
    func(update, context)
    update.message.reply_text.assert_called_once_with(NO_PERMISSIONS)

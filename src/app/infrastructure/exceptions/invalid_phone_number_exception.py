class InvalidPhoneNumberException(Exception):
    ERROR_MESSAGE = "Incorrect phone number format"

    def __init__(self):
        super().__init__(self.ERROR_MESSAGE)

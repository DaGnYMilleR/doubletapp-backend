def try_parse_int(value: str) -> [int, bool]:
    try:
        return int(value), True
    except ValueError:
        return value, False

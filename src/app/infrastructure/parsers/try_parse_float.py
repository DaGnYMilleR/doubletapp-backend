def try_parse_float(value: str) -> [float, bool]:
    try:
        return float(value), True
    except ValueError:
        return value, False

from injector import inject
from ninja import Router

from app.web_api.common.schemas.id_schema import IdSchema
from app.web_api.common.schemas.status_schemas import StatusSchema
from app.web_api.users.models.telegram_user_schema import TelegramUserSchema
from app.web_api.users.users_handlers import UsersHandlers


class UsersRouterFactory:
    @inject
    def __init__(self, handlers: UsersHandlers):
        self.handlers = handlers

    def create(self) -> Router:
        router = Router()

        router.add_api_operation("{user_id}", ["GET"], self.handlers.get_user_data, response=TelegramUserSchema)

        router.add_api_operation("{user_id}/phone", ["POST"], self.handlers.set_phone, response=IdSchema)

        router.add_api_operation("{user_id}", ["PUT"], self.handlers.edit_user, response=StatusSchema)

        router.add_api_operation("{user_id}/favorites", ["POST"], self.handlers.add_favorite, response=StatusSchema)

        router.add_api_operation(
            "{user_id}/favorites", ["DELETE"], self.handlers.delete_favorite, response=StatusSchema
        )

        return router

from ninja.orm import create_schema

from app.data_access.models.money_transfer import MoneyTransfer

MoneyTransferSchema = create_schema(MoneyTransfer, depth=1, exclude=["id"])

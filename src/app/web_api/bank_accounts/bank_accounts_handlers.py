from injector import inject
from ninja.errors import HttpError

from app.domain.services.bank_account_service import BankAccountService
from app.domain.services.statements_service import StatementsService


class BankAccountsHandlers:
    @inject
    def __init__(self, bank_account_service: BankAccountService, statements_service: StatementsService):
        self.statements_service = statements_service
        self.bank_account_service = bank_account_service

    def create_bank_acc(self, request, user_id: int):
        result = self.bank_account_service.create_bank_account(user_id)

        if not result.success:
            raise HttpError(400, result.error_message)

        return 201, {"id": result.value.id}

    def get_bank_acc_statement(self, request, bank_account_id: int):
        result = self.statements_service.get_bank_account_statement(bank_account_id)

        return result

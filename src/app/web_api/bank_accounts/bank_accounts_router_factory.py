from typing import List

from injector import inject
from ninja import Router

from app.web_api.bank_accounts.bank_accounts_handlers import BankAccountsHandlers
from app.web_api.bank_accounts.models.money_transfer_schema import MoneyTransferSchema
from app.web_api.common.schemas.id_schema import IdSchema


class BankAccountsRouterFactory:
    @inject
    def __init__(self, handlers: BankAccountsHandlers):
        self.handlers = handlers

    def create(self) -> Router:
        router = Router()

        router.add_api_operation("", ["POST"], self.handlers.create_bank_acc, response={201: IdSchema})

        router.add_api_operation(
            "{bank_account_id}/statement",
            ["GET"],
            self.handlers.get_bank_acc_statement,
            response=List[MoneyTransferSchema],
        )

        return router

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from injector import Injector
from ninja import NinjaAPI

from app.web_api.auth.auth_bearer import AuthBearer
from app.web_api.auth.auth_router_factory import AuthRouterFactory
from app.web_api.bank_accounts.bank_accounts_router_factory import BankAccountsRouterFactory
from app.web_api.users.users_router_factory import UsersRouterFactory

api = NinjaAPI()

injector = Injector()
auth_router = injector.get(AuthRouterFactory).create()
users_router = injector.get(UsersRouterFactory).create()
bank_accounts_router = injector.get(BankAccountsRouterFactory).create()

auth_bearer = injector.get(AuthBearer)

api.add_router("users/", users_router, tags=["users"], auth=auth_bearer)
api.add_router("auth/", auth_router, tags=["auth"])
api.add_router("bank-accounts/", bank_accounts_router, tags=["bank-accounts"])

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", api.urls),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

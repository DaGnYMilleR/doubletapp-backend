from django.contrib import admin

from app.data_access.models.money_transfer import MoneyTransfer


@admin.register(MoneyTransfer)
class MoneyTransfer(admin.ModelAdmin):
    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

from django.contrib import admin

from app.data_access.models.telegram_user import TelegramUser


@admin.register(TelegramUser)
class TelegramUserAdmin(admin.ModelAdmin):
    list_display = ("username", "first_name", "last_name", "phone_number")

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj, **kwargs)
        form.base_fields["favorites"].required = False
        return form

from django.contrib import admin

from app.data_access.models.card import Card


@admin.register(Card)
class CardAdmin(admin.ModelAdmin):
    pass

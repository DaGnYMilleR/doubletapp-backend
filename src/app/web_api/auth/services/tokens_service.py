import uuid
from datetime import datetime, timedelta
from typing import Any

import jwt
from django.db.models import Q
from jwt import InvalidTokenError

from app.data_access.models.issued_token import IssuedToken
from app.data_access.models.telegram_user import TelegramUser
from app.infrastructure.result import Fail, Result, Success
from app.web_api.auth.models.tokens import AccessToken, RefreshToken, TokensPair
from app.web_api.auth.schemas.tokens import RefreshTokenModel
from config.settings import (
    JWT_ACCESS_TOKEN_EXPIRATION_TIME_MINUTES,
    JWT_ALGORITHM,
    JWT_REFRESH_TOKEN_EXPIRATION_TIME_MINUTES,
    JWT_SECRET_KEY,
)

EXPIRATION_CLAIM = "exp"
JTI_CLAIM = "jti"
USER_ID_CLAIM = "user_id"


class TokensService:
    def generate_tokens(self, user: TelegramUser) -> TokensPair:
        access_token = self.create_access_token(user)
        refresh_token = self.create_refresh_token(user)

        return TokensPair(access_token, refresh_token)

    def refresh_tokens(self, model: RefreshTokenModel) -> Result[TokensPair]:
        payload = self.try_decode_token(model.refresh_token)
        if not payload.success:
            return Fail("incorrect token format")

        expired_time = payload.value[EXPIRATION_CLAIM]
        if expired_time < datetime.utcnow().timestamp():
            return Fail("token expired")

        token_id = payload.value[JTI_CLAIM]
        revoked_issued_token = (
            IssuedToken.objects.filter(Q(pk=token_id) & Q(revoked=True)).select_related("user").first()
        )
        if revoked_issued_token:
            self.revoke_all_user_tokens(revoked_issued_token.user)
            return Fail("forgery")

        issued_token = IssuedToken.objects.filter(Q(pk=token_id) & Q(revoked=False)).first()
        if not issued_token:
            return Fail("not logged in")

        issued_token.revoked = True
        issued_token.save()

        return Success(self.generate_tokens(issued_token.user))

    def revoke_all_user_tokens(self, user: TelegramUser):
        tokens = IssuedToken.objects.filter(Q(user__id=user.id) & Q(revoked=False))
        for token in tokens:
            token.revoked = True
            token.save()

    def try_decode_token(self, token: str) -> Result[dict[str, Any]]:
        try:
            payload = jwt.decode(token, key=JWT_SECRET_KEY, algorithms=[JWT_ALGORITHM], verify_exp=False)
            return Success(payload)
        except InvalidTokenError:
            return Fail()

    def create_access_token(self, user: TelegramUser) -> AccessToken:
        access_expires_at = (
            datetime.utcnow() + timedelta(minutes=JWT_ACCESS_TOKEN_EXPIRATION_TIME_MINUTES)
        ).timestamp()
        access_token = jwt.encode(
            payload={USER_ID_CLAIM: user.id, EXPIRATION_CLAIM: access_expires_at},
            key=JWT_SECRET_KEY,
            algorithm=JWT_ALGORITHM,
        )

        return AccessToken(access_token, access_expires_at)

    def create_refresh_token(self, user: TelegramUser) -> RefreshToken:
        refresh_expires_at = (
            datetime.utcnow() + timedelta(hours=JWT_REFRESH_TOKEN_EXPIRATION_TIME_MINUTES)
        ).timestamp()
        jti = str(uuid.uuid4())
        refresh_token = jwt.encode(
            payload={JTI_CLAIM: jti, EXPIRATION_CLAIM: refresh_expires_at},
            key=JWT_SECRET_KEY,
            algorithm=JWT_ALGORITHM,
        )

        IssuedToken.objects.create(jti=jti, user=user)

        return RefreshToken(refresh_token, refresh_expires_at)

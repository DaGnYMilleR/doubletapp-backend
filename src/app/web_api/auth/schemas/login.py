from ninja import Schema


class LoginModel(Schema):
    login: int
    password: str

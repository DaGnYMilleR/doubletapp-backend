from ninja import Schema


class AccessTokenSchema(Schema):
    token: str
    expires_at: float


class RefreshTokenSchema(Schema):
    token: str
    expires_at: float


class TokensPairSchema(Schema):
    access_token: AccessTokenSchema
    refresh_token: RefreshTokenSchema


class RefreshTokenModel(Schema):
    refresh_token: str

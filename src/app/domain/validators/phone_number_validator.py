import re

PHONE_NUMBER_REGEX = re.compile(r"^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$")


class PhoneNumberValidator:
    @staticmethod
    def is_valid(phone_number: str) -> bool:
        return re.fullmatch(PHONE_NUMBER_REGEX, phone_number) is not None

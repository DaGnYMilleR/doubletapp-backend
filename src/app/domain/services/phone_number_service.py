from injector import inject

from app.data_access.models.telegram_user import TelegramUser
from app.domain.services.user_service import UserService
from app.domain.validators.phone_number_validator import PhoneNumberValidator
from app.infrastructure.message_constants.error_messages import INVALID_PHONE_NUMBER_FORMAT
from app.infrastructure.message_constants.response_messages import PHONE_SAVED
from app.infrastructure.result import Fail, Result, Success


class PhoneNumberService:
    @inject
    def __init__(self, user_service: UserService):
        self.user_service = user_service

    def set_user_phone(self, user_id: int, phone_number: str) -> Result[str]:
        user_result = self.user_service.get_user(user_id)

        if not user_result.success:
            return Fail(user_result.error_message)

        if not PhoneNumberValidator.is_valid(phone_number):
            return Fail(INVALID_PHONE_NUMBER_FORMAT)

        user = user_result.value

        user.phone_number = phone_number
        user.save()
        return Success(PHONE_SAVED)

    @staticmethod
    def is_phone_filled(user_id: int) -> bool:
        phone = TelegramUser.objects.filter(id=user_id).values_list("phone_number").first()

        return phone is not None

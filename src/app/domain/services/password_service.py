from django.contrib.auth.hashers import check_password, make_password

from app.data_access.models.telegram_user import TelegramUser


class PasswordService:
    def make_password(self, user_id: int, password: str) -> str:
        return make_password(password)

    def check_password(self, user: TelegramUser, password: str) -> bool:
        user_password = user.password_hash
        return user_password is not None and check_password(password, user_password)

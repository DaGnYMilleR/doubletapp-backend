from functools import wraps

from telegram import Update
from telegram.ext import CallbackContext

from app.infrastructure.message_constants.error_messages import INCORRECT_ARGUMENTS_NUMBER


def require_args(args_len):
    args_len = [args_len] if isinstance(args_len, int) else args_len

    def wrapper(handler):
        @wraps(handler)
        def wrpr(self, update: Update, context: CallbackContext):
            if len(context.args) not in args_len:
                return update.message.reply_text(INCORRECT_ARGUMENTS_NUMBER)
            return handler(self, update, context)

        return wrpr

    return wrapper

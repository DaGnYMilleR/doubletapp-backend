from functools import wraps

from telegram import Update
from telegram.ext import CallbackContext

from app.domain.services.phone_number_service import PhoneNumberService
from app.infrastructure.message_constants.error_messages import NO_PERMISSIONS


def phone_required(telegram_command_handler):
    @wraps(telegram_command_handler)
    def wrapper(self, update: Update, context: CallbackContext):
        user_id = update.message.from_user.id

        if not PhoneNumberService.is_phone_filled(user_id):
            update.message.reply_text(NO_PERMISSIONS)
        else:
            telegram_command_handler(self, update, context)

    return wrapper

from injector import inject
from telegram import Update
from telegram.ext import CallbackContext

from app.data_access.models.telegram_user import TelegramUser
from app.domain.services.favorite_users_service import FavoriteUsersService
from app.infrastructure.message_constants.error_messages import NOT_A_NUMBER
from app.infrastructure.parsers.try_parse_int import try_parse_int
from app.telegram_bot.decorators.phone_required import phone_required
from app.telegram_bot.decorators.require_args import require_args


class FavoriteUsersHandlers:
    @inject
    def __init__(self, favorite_users_service: FavoriteUsersService):
        self.favorite_users_service = favorite_users_service

    @phone_required
    @require_args(1)
    def cmd_add_to_favorites(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id
        args = context.args

        other_user_id, parsed = try_parse_int(args[0])
        if not parsed:
            update.message.reply_text(NOT_A_NUMBER.format("other user id"))
            return

        result = self.favorite_users_service.add_user_to_favorites(user_id, other_user_id)
        if not result.success:
            update.message.reply_text(result.error_message)
            return

        update.message.reply_text(result.value)

    @phone_required
    def cmd_get_favorites_list(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id

        favorites_result = self.favorite_users_service.get_favorites(user_id)
        if not favorites_result.success:
            update.message.reply_text(favorites_result.error_message)
            return

        favorites = map(self.map_to_view, favorites_result.value)
        update.message.reply_text("\n\n".join(favorites))

    @phone_required
    @require_args(1)
    def cmd_delete_favorite(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id
        args = context.args

        other_user_id, parsed = try_parse_int(args[0])
        if not parsed:
            update.message.reply_text(NOT_A_NUMBER.format("other user id"))
            return

        result = self.favorite_users_service.delete_favorite_user(user_id, other_user_id)
        if not result.success:
            update.message.reply_text(result.error_message)
            return

        update.message.reply_text(result.value)

    @staticmethod
    def map_to_view(user: TelegramUser) -> str:
        return f"name: {user.username}\nid: {user.id}"

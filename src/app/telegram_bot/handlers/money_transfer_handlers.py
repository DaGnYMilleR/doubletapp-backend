from injector import inject
from telegram import Update
from telegram.ext import CallbackContext

from app.data_access.models.money_transfer import MoneyTransfer
from app.domain.services.money_transfer_service import MoneyTransferService
from app.domain.services.statements_service import StatementsService
from app.domain.services.user_service import UserService
from app.infrastructure.message_constants.error_messages import (
    BANK_ACCOUNT_DOES_NOT_BELONGS_TO_USER,
    CARD_DOES_NOT_BELONGS_TO_USER,
    INCORRECT_ARGUMENTS_NUMBER,
    NOT_A_NUMBER,
)
from app.infrastructure.parsers.try_parse_float import try_parse_float
from app.infrastructure.parsers.try_parse_int import try_parse_int
from app.telegram_bot.decorators.phone_required import phone_required
from app.telegram_bot.decorators.require_args import require_args


class MoneyTransferHandlers:
    @inject
    def __init__(
        self,
        statements_service: StatementsService,
        money_transfer_service: MoneyTransferService,
        user_service: UserService,
    ):
        self.user_service = user_service
        self.money_transfer_service = money_transfer_service
        self.statements_service = statements_service

    @phone_required
    @require_args([0, 1])
    def cmd_money(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id
        args = context.args

        if len(args) == 0:
            self.handle_bank_account_balance(update, user_id)
            return

        if len(args) == 1:
            self.handle_card_balance(update, user_id, args[0])
            return

        update.message.reply_text(INCORRECT_ARGUMENTS_NUMBER)

    @phone_required
    @require_args(3)
    def cmd_transfer_user_money(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id
        args = context.args

        self.update_balance_by_card_number(update, user_id, args[0], args[1], args[2])

    @phone_required
    @require_args(1)
    def cmd_get_card_statement(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id
        card_id, success = try_parse_int(context.args[0])

        if not success:
            update.message.reply_text(NOT_A_NUMBER.format("card_id"))
            return

        if not self.user_service.is_card_belongs_to_user(user_id, card_id):
            update.message.reply_text(CARD_DOES_NOT_BELONGS_TO_USER)
            return

        result = self.statements_service.get_card_statement(card_id)
        string_result = map(self.transform_statement, result)
        update.message.reply_text("\n".join(string_result))

    @phone_required
    @require_args(1)
    def cmd_get_bank_account_statement(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id
        bank_account_id, success = try_parse_int(context.args[0])

        if not success:
            update.message.reply_text(NOT_A_NUMBER.format("bank_account_id"))
            return

        if not self.user_service.is_bank_account_belongs_to_user(user_id, bank_account_id):
            update.message.reply_text(BANK_ACCOUNT_DOES_NOT_BELONGS_TO_USER)
            return

        result = self.statements_service.get_bank_account_statement(bank_account_id)
        string_result = map(self.transform_statement, result)
        update.message.reply_text("\n".join(string_result))

    @phone_required
    def cmd_get_statements_usernames(self, update: Update, context: CallbackContext) -> None:
        user_id = update.message.from_user.id

        result = self.statements_service.get_statement_users_names(user_id)

        update.message.reply_text("\n".join(result))

    def transform_statement(self, transfer: MoneyTransfer) -> str:
        return (
            f"{transfer.from_card.bank_account.user.username} "
            f"-> {transfer.to_card.bank_account.user.username}: {transfer.amount}"
        )

    def update_balance_by_card_number(
        self, update: Update, user_id: int, from_card_id: str, to_card_id: str, amount: str
    ) -> None:
        from_card_id, parsed = try_parse_int(from_card_id)
        if not parsed:
            update.message.reply_text(NOT_A_NUMBER.format("from_card"))
            return

        to_card_id, parsed = try_parse_int(to_card_id)
        if not parsed:
            update.message.reply_text(NOT_A_NUMBER.format("to_card"))
            return

        amount, parsed = try_parse_float(amount)
        if not parsed:
            update.message.reply_text(NOT_A_NUMBER.format("amount"))
            return

        transfer_result = self.money_transfer_service.transfer_money(user_id, from_card_id, to_card_id, amount)

        if not transfer_result.success:
            update.message.reply_text(transfer_result.error_message)
            return

        update.message.reply_text(transfer_result.value)

    def handle_card_balance(self, update: Update, user_id: int, card_id: str) -> None:
        card_id, parsed = try_parse_int(card_id)
        if not parsed:
            update.message.reply_text(NOT_A_NUMBER.format("card_id"))
            return

        card_balance_result = self.user_service.get_card_balance(user_id, card_id)

        if not card_balance_result.success:
            update.message.reply_text(card_balance_result.error_message)
            return

        update.message.reply_text(f"card balance: {card_balance_result.value}")

    def handle_bank_account_balance(self, update: Update, user_id: int) -> None:
        balance_result = self.user_service.get_bank_account_balance(user_id)

        if not balance_result.success:
            update.message.reply_text(balance_result.error_message)
            return

        update.message.reply_text(f"balance: {balance_result.value}")

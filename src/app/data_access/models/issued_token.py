from django.db import models
from django.utils import timezone

from app.data_access.models.telegram_user import TelegramUser


class IssuedToken(models.Model):
    jti = models.CharField(max_length=255, primary_key=True)
    user = models.ForeignKey(TelegramUser, on_delete=models.CASCADE, related_name="refresh_tokens")
    created_at = models.DateTimeField(default=timezone.now)
    revoked = models.BooleanField(default=False)

from django.core.validators import RegexValidator
from django.db import models

from app.domain.validators.phone_number_validator import PHONE_NUMBER_REGEX


class TelegramUser(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    username = models.CharField(max_length=20, unique=True, null=True)
    first_name = models.CharField(max_length=20, null=True)
    last_name = models.CharField(max_length=20, null=True)
    phone_number = models.CharField(
        max_length=16, unique=True, null=True, validators=[RegexValidator(regex=PHONE_NUMBER_REGEX)]
    )
    favorites = models.ManyToManyField("self", symmetrical=False)
    password_hash = models.CharField(max_length=100, null=True)

    def __str__(self):
        return (
            f"id: {self.id}\nusername: {self.username}\nfirst_name: {self.first_name}\nlast_name: {self.last_name}"
            f"\nphone: {self.phone_number}"
        )

migrate:
	docker-compose run --rm web python manage.py migrate $(if $m, api $m,)

makemigrations:
	docker-compose run --rm web python manage.py makemigrations

createsuperuser:
	docker-compose run --rm web python manage.py createsuperuser

collectstatic:
	docker-compose run --rm web python manage.py collectstatic --no-input

build:
	docker-compose build

dev:
	docker-compose up -d

push:
	docker push ${IMAGE_APP}

pull:
	docker pull ${IMAGE_APP}


command:
	docker-compose run --rm web python manage.py ${c}

shell:
	docker-compose run --rm web python manage.py shell

debug:
	docker-compose run --rm web python manage.py debug

piplock:
	pipenv install
	chown -R ${USER} Pipfile.lock

lint:
	black --config pyproject.toml .
	isort .
	flake8 --config setup.cfg

check_lint:
	pipenv run black --check --config pyproject.toml .

build_image:
	docker image build -t dt-app:dev .

down:
	docker-compose down

db_init:
	docker-compose run --rm db

docker_lint:
	docker-compose run --rm web python manage.py check_lint

ci_check_lint:
	docker run --rm ${IMAGE_APP}
	docker-compose run --rm web isort --check --diff .
	docker-compose run --rm web flake8 --config setup.cfg
	docker-compose run --rm web black --check --config pyproject.toml .

test_all:
	docker-compose run --rm web pytest --import-mode=importlib

test_unit:
	docker-compose run --rm web pytest test/unit --import-mode=importlib

test_integration:
	docker-compose run --rm web pytest test/integration --import-mode=importlib



